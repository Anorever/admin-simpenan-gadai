<?php 
  error_reporting(0);
	session_start();
	if($_SESSION['status']!="login"){
        echo "<script>alert('Anda Belum Login!');window.location='../login.php';</script>";
	}
	?>

<?php
  // memanggil file koneksi.php untuk membuat koneksi
include 'koneksi.php';

  // mengecek apakah di url ada nilai GET id
  if ($_GET['id']==true) {
    // ambil nilai id dari url dan disimpan dalam variabel $id
    $id = ($_GET["id"]);

    // menampilkan data dari database yang mempunyai id=$id
    $query = "SELECT * FROM user_sim_gade WHERE id='$id'";
    $result = mysqli_query($koneksi, $query);
    // jika data gagal diambil maka akan tampil error berikut
    if(!$result){
      die ("Query Error: ".mysqli_errno($koneksi).
         " - ".mysqli_error($koneksi));
    }
    // mengambil data dari database
    $data = mysqli_fetch_assoc($result);
      // apabila data tidak ada pada database maka akan dijalankan perintah ini
       if (!count($data)) {
          echo "<script>alert('Data tidak ditemukan pada database');window.location='user_config.php';</script>";
       }
  } else {
    // apabila tidak ada data GET id pada akan di redirect ke index.php
    echo "<script>alert('Masukkan data id.');window.location='user_config.php';</script>";
  }         
  ?>

<!DOCTYPE html>
<html>
  <head>
  <title>SIMPENAN GADE DASHBOARD</title>
        <link href="css/styles.css" rel="stylesheet" />
        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'GA_MEASUREMENT_ID');
        </script>
<style type="text/css">
      * {
        font-family: "Trebuchet MS";
      }
      h1 {
        text-transform: uppercase;
        /* color: salmon; */
      }
 
    label {
      margin-top: 10px;
      float: left;
      text-align: left;
      width: 100%;
    }
    input {
      padding: 6px;
      width: 100%;
      box-sizing: border-box;
      background: #f8f8f8;
      border: 2px solid #ccc;
      /* outline-color: salmon; */
    }

    .base {
      width: 400px;
      height: auto;
      padding: 20px;
      margin-left: auto;
      margin-right: auto;
      background: #ededed;
    }
    </style>
</head>
  <body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="index.php">SIMPENAN GADAI</a><button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button
            ><!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <!-- <a class="dropdown-item" href="#">Settings</a><a class="dropdown-item" href="#">Activity Log</a>
                        <div class="dropdown-divider"></div> -->
                        <a class="dropdown-item" href="../logout.php">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Monitoring</div>
                            <a class="nav-link" href="../index.php" onclick="alert('Klik Cancel untuk membatalkan Edit!'); return false;"
                                ><div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Traffic Visitor</a
                            >
                            <a class="nav-link" href="../trends.php" onclick="alert('Klik Cancel untuk membatalkan Edit!'); return false;"
                                ><div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Traffic Trends</a
                            >
                            <div class="sb-sidenav-menu-heading">Data Update</div>
                            <a class="nav-link" href="../grafik.php" onclick="alert('Klik Cancel untuk membatalkan Edit!'); return false;"
                                ><div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                                HPS Grafik</a
                            ><a class="nav-link" href="../data.php" onclick="alert('Klik Cancel untuk membatalkan Edit!'); return false;"
                                ><div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Data Input</a
                            >
                            <div class="sb-sidenav-menu-heading">GIST</div>
                            <a class="nav-link" href="user_config.php" onclick="alert('Klik Cancel untuk membatalkan Edit!'); return false;"
                                ><div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Management User</a
                            >
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        SimGade-Admin
                    </div>
                </nav>
                </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">USER SIMPENAN-GADAI</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Kumpulan User Simpenan Gadai</li>
                        </ol>
                        <div class="container text-center">
      <center>
        <h1>Edit User <?php echo $data['user']; ?></h1>
      <center>
      <form method="POST" action="proses_edit.php" enctype="multipart/form-data" >
      <section class="base">
        <!-- menampung nilai id produk yang akan di edit -->
        <input name="id" value="<?php echo $data['id']; ?>"  hidden />
        <div>
          <label>User</label>
          <input type="text" name="user" value="<?php echo $data['user']; ?>" autofocus="" required="" />
        </div>
        <div>
          <label>Nama</label>
          <input type="text" name="nama" value="<?php echo $data['Nama']; ?>" autofocus="" required="" />
        </div>
        <div>
          <label>Password</label>
         <input type="password" name="password" value="<?php echo $data['password']; ?>" />
        </div>
        <div>
          <label>Cabang</label>
         <input type="text" name="cabang" required=""value="<?php echo $data['cabang']; ?>" />
        </div>
        <div style="padding-top:2%;">
        <a href="javascript:history.go(-1)" class="btn btn-secondary">Cancel</a>
         <button type="submit"  class="btn btn-secondary">Simpan User</button>
        </div>
        </section>
      </form>

      <script>
      function click_cancel(){
        alert("Klik Cancel untuk membatalkan Edit!");
        return false;
      }
  </body>
</html>