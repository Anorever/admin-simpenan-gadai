<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>LOGIN SIMPENAN-GADAI</title>
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <style>
        body{
            background : url('images/header1.jpg');
            background-size:     cover;                      
            background-repeat:   no-repeat;
            background-position: center center; 
        }
    </style>
    <body >
    
    <div class="row">
        <div class="col-lg-4">
            <img src="images/Logo_SG.png" width="700px" style="padding-top:40%; padding-left:20%;">
        </div>
        <div class="col-lg-8" style="align:right;">
        <div id="layoutAuthentication" style="padding-top:7%; ">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Login</h3></div>
                                    <div class="card-body">
                                        <form action="login_proses.php" method="post">
                                            <div class="form-group"><label class="small mb-1" for="inputEmailAddress">User ID</label><input required name="user" class="form-control py-4" id="inputEmailAddress" type="text" placeholder="Enter User ID" /></div>
                                            <div class="form-group"><label class="small mb-1" for="inputPassword">Password</label><input required name="pass" class="form-control py-4" id="inputPassword" type="password" placeholder="Enter password" /></div>
                                            <div class="form-group">
                                                <div class="custom-control custom-checkbox"><input class="custom-control-input" id="rememberPasswordCheck" type="checkbox" /><label class="custom-control-label" for="rememberPasswordCheck">Remember password</label></div>
                                            </div>
                                            <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0"><a class="small" href="password.php">Forgot Password?</a>
                                                <!-- <a class="btn btn-primary" href="index.php">Login</a> -->
                                                <button class="btn btn-primary" tyoe="submit">Login</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>
