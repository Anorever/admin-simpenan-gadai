<!DOCTYPE html>
<html lang="en">
    <head>
        <title>SIMPENAN GADE DASHBOARD</title>
        <link href="css/styles.css" rel="stylesheet" />
        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'GA_MEASUREMENT_ID');
        </script>
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    </head>
    <?php
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname2 = "sim-gade";
    
        $conn2 = new mysqli($servername, $username, $password, $dbname2);
        if ($conn2->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        // $wilayah   = mysqli_query($conn2, "SELECT wilayah FROM halaman_utama_input order by ID asc");
        // $nominal = mysqli_query($conn2, "SELECT nominal FROM halaman_utama_input order by ID asc");

        $wilayah = mysqli_query($conn2, "SELECT wilayah FROM halaman_utama_input GROUP BY wilayah order by wilayah desc");
        $nominal = mysqli_query($conn2, "SELECT count(*) as nominal FROM halaman_utama_input GROUP BY wilayah desc");
        // $wilayah2  = mysqli_query($conn2, "SELECT estimasi_taksir FROM button_detail_product order by ID asc");
        // $wilayah2  = mysqli_query($conn2, "SELECT count(*) jumlah FROM button_detail_product group by merek order by ID asc");
        // $jum_nominal = mysqli_query($conn2, "SELECT count(*) jum_nominal FROM button_detail_product group by estimasi_taksir order by ID asc");
        
        $merek =mysqli_query($conn2, "SELECT CONCAT(merek, '=Rp.', estimasi_taksir) as merek FROM button_detail_product GROUP BY merek order by merek desc");
        $wilayah2 =mysqli_query($conn2, "SELECT count(*) as jumlah FROM button_detail_product GROUP BY merek order by merek desc");
       
        $estimasi_detail = mysqli_query($conn2, "SELECT CONCAT(wilayah, '=Rp.', nominal) as estimasi_taksir FROM halaman_utama_input GROUP BY wilayah order by nominal desc");
        $jum_nominal = mysqli_query($conn2, "SELECT count(*) as jum_nominal FROM halaman_utama_input GROUP BY wilayah order by nominal desc");
        
        
        // $estimasi_detail  = mysqli_query($conn2, "SELECT estimasi_taksir FROM button_detail_product order by ID asc");
        // $merek = mysqli_query($conn2, "SELECT merek FROM button_detail_product order by ID asc");
    ?>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="index.php">SIMPENAN GADAI</a><button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button
            ><!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <!-- <a class="dropdown-item" href="#">Settings</a><a class="dropdown-item" href="#">Activity Log</a>
                        <div class="dropdown-divider"></div> -->
                        <a class="dropdown-item" href="login.html">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Monitoring</div>
                            <a class="nav-link" href="index.php"
                                ><div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Traffic Visitor</a
                            >
                            <a class="nav-link" href="trends.php"
                                ><div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Traffic Trends</a
                            >
                            <div class="sb-sidenav-menu-heading">Data Update</div>
                            <a class="nav-link" href="grafik.php"
                                ><div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                                HPS Grafik</a
                            ><a class="nav-link" href="data2.php"
                                ><div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Data Input</a
                            >
                            <div class="sb-sidenav-menu-heading">GIST</div>
                            <a class="nav-link" href="crud/user_config.php"
                                ><div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Management User</a
                            >
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        SimGade-Admin
                    </div>
                </nav>
            </div>
    
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Traffic Trends</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Persentase dari inputan pengguna meliputi merek, tipe dan Inputan Nominal Nasabah</li>
                        </ol>
                        <div class="container text-center">
                            <div class="row text-center">
                                <div class="col-lg-6 text-center">
                                    <div class="card">
                                        <div class="card-header"><i class="fas fa-chart-pie mr-1"></i>Wilayah</div>
                                        <div class="card-body"><canvas id="wilayahGraph" width="400" height="400"></canvas> </div>  
                                        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
                                    </div>
                                </div>
                                <div class="col-lg-6 text-center">
                                    <div class="card">
                                        <div class="card-header"><i class="fas fa-chart-pie mr-1"></i>Tipe</div>
                                        <!-- <div class="card-body"><canvas id="myPieChart" width="100%" height="50"></canvas></div> -->
                                        <div class="card-body"><canvas id="merekGraph" width="400" height="400"></canvas> </div>  
                                        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="container" style="padding-left:20%; padding-top:3%; padding-right:20%;">
                        <div class="text-center">
                            <div class="card">
                            <div class="card-header"><i class="fas fa-chart-bar mr-1"></i>Inputan Nominal Nasabah</div>
                            <div class="card-body"><canvas id="nominalNasabah" width="400" height="400"></canvas> </div>  
                            <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
                            </div>
                        </div>
                        </div>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Team4Sprint</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script  type="text/javascript">
        var options = {
            scales: {
                xAxes: [{
                ticks: {
                    callback: function(value) {
                    if (value.length > 4) {
                        return value.substr(0, 4) + '...'; //truncate
                    } else {
                        return value
                    }

                    },
                }
                }],
                yAxes: [{}]
            },
            tooltips: {
                enabled: true,
                mode: 'label',
                callbacks: {
                title: function(tooltipItems, data) {
                    var idx = tooltipItems[0].index;
                    return 'Title:' + data.labels[idx]; //do something with title
                },
                label: function(tooltipItems, data) {
                    //var idx = tooltipItems.index;
                    //return data.labels[idx] + ' €';
                    return tooltipItems.xLabel;
                }
                }
            },
            };


    	  var ctx = document.getElementById("wilayahGraph").getContext("2d");
    	  var data = {
    	            labels: [<?php while ($p = mysqli_fetch_array($wilayah)) { echo '"' . $p['wilayah'] . '",';}?>],
    	            datasets: [
    	            {
    	              label: "Wilayah",
    	              data: [<?php while ($p = mysqli_fetch_array($nominal)) { echo '"' . $p['nominal'] . '",';}?>],
                    backgroundColor: [
                      "rgba(59, 100, 222, 1)",
                      "rgba(203, 222, 225, 0.9)",
                      "rgba(102, 50, 179, 1)",
                      "rgba(201, 29, 29, 1)",
                      "rgba(81, 230, 153, 1)",
                      "rgba(246, 34, 19, 1)",
                      "rgba(246, 84, 19, 1)",
                      "rgba(246, 114, 19, 1)",
                      "rgba(46, 34, 119, 1)",
                      "rgba(46, 200, 19, 1)",
                      "rgba(246, 134, 101, 1)",
                      "rgba(46, 54, 29, 1)",
                      "rgba(146, 34, 19, 1)",
                      "rgba(246, 4, 19, 1)",
                      "rgba(246, 4, 19, 1)",
                        "rgba(46, 100, 19, 1)",
                        "rgba(6, 104, 101, 1)",
                        "rgba(17, 54, 29, 1)",
                        "rgba(146, 34, 100, 1)",
                        "rgba(246, 400, 19, 1)",
                        "rgba(49, 200, 19, 1)",
                        "rgba(26, 14, 101, 1)",
                        "rgba(46, 4, 29, 1)",
                        "rgba(146, 34, 1, 1)",
                        "rgba(26, 4, 1, 1)",
                      ]
    	            }
    	            ]
    	            };

    	  var myBarChart = new Chart(ctx, {
    	            type: 'bar',
    	            data: data,
    	            options: {
                    responsive: true
    	          },options: options
    	        });
    	</script>

        <script  type="text/javascript">

        var ctx = document.getElementById("merekGraph").getContext("2d");
        var data = {
                labels: [<?php while ($p = mysqli_fetch_array($merek)) { echo '"' . $p['merek'] . '",';}?>],
                datasets: [
                {
                    label: "Pencarian Merek",
                    data: [<?php while ($p = mysqli_fetch_array($wilayah2)) { echo '"' . $p['jumlah'] . '",';}?>],
                backgroundColor: [
                    "rgba(59, 100, 222, 1)",
                    "rgba(203, 222, 225, 0.9)",
                    "rgba(102, 50, 179, 1)",
                    "rgba(201, 29, 29, 1)",
                    "rgba(81, 230, 153, 1)",
                    "rgba(246, 34, 19, 1)",
                    "rgba(246, 84, 19, 1)",
                    "rgba(246, 114, 19, 1)",
                    "rgba(46, 34, 119, 1)",
                    "rgba(46, 200, 19, 1)",
                    "rgba(246, 134, 101, 1)",
                    "rgba(46, 54, 29, 1)",
                    "rgba(146, 34, 19, 1)",
                    ]
                }
                ]
                };

        var myBarChart = new Chart(ctx, {
                type: 'pie',
                data: data,
                options: {
                responsive: true
                }
            });
        </script>
        <script  type="text/javascript">

        var ctx = document.getElementById("nominalNasabah").getContext("2d");
        var data = {
                labels: [<?php while ($p = mysqli_fetch_array($estimasi_detail)) { echo '"' . $p['estimasi_taksir'] . '",';}?>],
                datasets: [
                {
                    label: "Pilihan Nominal Nasabah",
                    data: [<?php while ($p = mysqli_fetch_array($jum_nominal)) { echo '"' . $p['jum_nominal'] . '",';}?>],
                backgroundColor: [
                    "rgba(81, 230, 153, 1)",
                    "rgba(246, 34, 19, 1)",
                    "rgba(246, 84, 19, 1)",
                    "rgba(246, 114, 19, 1)",
                    "rgba(46, 34, 119, 1)",
                    "rgba(46, 200, 19, 1)",
                    "rgba(246, 134, 101, 1)",
                    "rgba(46, 54, 29, 1)",
                    "rgba(146, 34, 19, 1)",
                    "rgba(246, 4, 19, 1)",
                    "rgba(46, 100, 19, 1)",
                        "rgba(6, 104, 101, 1)",
                        "rgba(17, 54, 29, 1)",
                        "rgba(146, 34, 100, 1)",
                        "rgba(246, 400, 19, 1)",
                        "rgba(49, 200, 19, 1)",
                        "rgba(26, 14, 101, 1)",
                        "rgba(46, 4, 29, 1)",
                        "rgba(146, 34, 1, 1)",
                        "rgba(26, 4, 1, 1)",
                    ]
                }
                ]
                };

        var myBarChart = new Chart(ctx, {
                type: 'pie',
                data: data,
                options: {
                responsive: true
                }
            });
        </script>


        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/chart-area-demo.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/datatables-demo.js"></script>
        <script src="assets/demo/chart-pie-demo.js"></script>
    </body>
</html>
