<!DOCTYPE html>
<html lang="en">
<head>

     <title>SIMPENAN GADAI</title>

     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

     <link rel="stylesheet" href="css/bootstrap.min.css">
     <link rel="stylesheet" href="css/font-awesome.min.css">
     <link rel="stylesheet" href="css/animate.css">
     <link rel="stylesheet" href="css/owl.carousel.css">
     <link rel="stylesheet" href="css/owl.theme.default.min.css">
     <link rel="stylesheet" href="css/magnific-popup.css">

     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
     <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
     <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

     <!-- MAIN CSS -->
     <link rel="stylesheet" href="css/templatemo-style2.css">
    
</head>
<body>
  
    <?php
    // $var = 1;
    $var = $_POST['layout_select'];
    $var2 = $_POST['versi_select'];
    $pembayaran = $_POST['pembayaran'];

    //Rest of the vars

    $col = $_POST['column_select']; 
    $layout = $_POST['layout_select'];
    $type = $_POST['type_select'];
    $warna = floatval($_POST['warna_select']);
    if($warna=="25"){
         $warna_desc="Putih";
    }
    else if($warna=="50"){
          $warna_desc="Gold";
    }
    else if($warna=="75"){
          $warna_desc="Pink";
    }

    $ver = $_POST['versi_select'];
    if($ver=="0"){
          $ver_desc="< Kitkat";
     }
    else if($ver=="25"){
          $ver_desc="Kitkat";
     }
    else if($ver=="50"){
          $ver_desc="Lollipop";
     }
    else if($ver=="75"){
          $ver_desc="Marshmallow";
     }
    else if($ver=="100"){
          $ver_desc="> Nougat";
     }

    $jns_bayar = $_POST['pembayaran'];
     if($jns_bayar=="1"){
          $jns_bayar_desc="Lunas";
     }
     else if($jns_bayar=="0"){
          $jns_bayar_desc="Cicilan";
     }

    // echo($col.$layout.$type.$warna.$ver.$jns_bayar."\n");

    $tP1 = $_POST['tampilan_depan1'];
     if($tP1=="100"){
          $tP1_desc = "Lampu Indikator Hidup";
     }
     else if($tP1=="0"){
          $tP1_desc = "Lampu Indikator Mati";
     }
     else if($tP1=="75"){
          $tP1_desc = "Tidak Terdapat Lampu Indikator";
     }
    $tP2 = $_POST['tampilan_depan2'];
     if($tP2=="100"){
          $tP2_desc = "Port USB Berfungsi";
     }
     else if($tP2=="0"){
          $tP2_desc = "Port USB Tidak Berfungsi";
     }

    $tP3 = $_POST['tampilan_depan3'];
     if($tP3=="100"){
          $tP3_desc = "Layar Baik";
     }
     else if($tP3=="0"){
          $tP3_desc = "Layar Retak";
     }
     else if($tP3=="25"){
          $tP3_desc = "Layar Terdapat Bintik Hitam";
     }

    // echo($tP1.$tP2.$tP3."\n");

    $tB1 = $_POST['tampilan_belakang1'];
    if($tB1=="100"){
     $tB1_desc = "Kamera Berfungsi";
     }
     else if($tB1=="0"){
          $tB1_desc = "Kamera Tidak Berfungsi";
     }
    $tB2 = $_POST['tampilan_belakang2'];
     if($tB2=="100"){     
          $tB2_desc = "Tombol Berfungsi";
     }
     else if($tB2=="0"){
          $tB2_desc = "Tombol Tidak Berfungsi";
     }

    $tB3 = $_POST['tampilan_belakang3'];
     if($tB3=="100"){
          $tB3_desc = "Lampu Flash Hidup";
     }
     else if($tB3=="0"){
          $tB3_desc = "Lampu Flash Mati";
     }

    $tB4 = $_POST['tampilan_belakang4'];
     if($tB4=="100"){
          $tB4_desc = "Casing Baik";
     }
     else if($tB4=="50"){
          $tB4_desc = "Casing Lecet";
     }
     else if($tB4=="0"){
          $tB4_desc = "Casing Retak";
     }


    // echo($tB1.$tB2.$tB3.$tB4."\n");

    $K1 = $_POST['kelengkapan1'];
     if($K1=="100"){
          $K1_desc = "Kotak Ada";
     }
     else if($K1=="75"){
          $K1_desc = "Kotak Tidak Ada";
     }

    $K2 = $_POST['kelengkapan2'];
     if($K2=="100"){
        $K2_desc = "Baterai Baik";
     }
     else if($K2=="75"){
          $K2_desc = "Baterai Rusak";
     }
    $K3 = $_POST['kelengkapan3'];
    if($K3=="100"){
          $K3_desc = "Chager Baik dan Normal";
     }
     else if($K3=="0"){
          $K3_desc = "Chager Rusak/Hilang";
     }

    // echo($K1.$K2.$K3."\n");

     // $tipe = substr($_POST['tipe'],13);
     $merek = '';
     if (strpos($type, 'Samsung') !== false) {
       $merek='Samsung';
     }
     else if (strpos($type, 'Iphone') !== false) {
       $merek='Iphone';
     }
     else if (strpos($type, 'Xiaomi') !== false) {
       $merek='Xiaomi';
     }
     else if (strpos($type, 'Oppo') !== false) {
       $merek='Oppo';
     }
     else if (strpos($type, 'Vivo') !== false) {
       $merek='Vivo';
     }

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname2 = "sim-gade";
    // Create connection
    $conn2 = new mysqli($servername, $username, $password, $dbname2);
    // Check connection
    if ($conn2->connect_error) {
        echo (die("Connection failed: " . $conn2->connect_error));
    }
    $idpenaksir="P12345";
    $layak="Layak";
    $sql = 'INSERT INTO fuzzy_data_penaksir (id_penaksir,produk,merek,tipe,warna,versi_android,jenis_bayar,tampilan_depan1,tampilan_depan2,tampilan_depan3,tampilan_belakang1,tampilan_belakang2,tampilan_belakang3,tampilan_belakang4,kelengkapan1,kelengkapan2,kelengkapan3,Kelayakan) VALUES
    ("'.$idpenaksir.'","'.$col.'","'.$layout.'","'.$type.'","'.$warna_desc.'","'.$ver_desc.'","'.$jns_bayar_desc.'","'.$tP1_desc.'","'.$tP2_desc.'","'.$tP3_desc.'","'.$tB1_desc.'","'.$tB2_desc.'","'.$tB3_desc.'","'.$tB4_desc.'","'.$K1_desc.'","'.$K2_desc.'","'.$K3_desc.'","'.$layak.'")';
    mysqli_query($conn2, $sql);

    $fuzzy = exec("python fuzzy.py $warna $ver $tP1 $tP2 $tP3 $tB1 $tB2 $tB3 $tB4 $K2 $K3");
    $regresi = exec("python regression.py");


    if($fuzzy<40){
        $layak="Tidak Layak";
        $sql = 'INSERT INTO fuzzy_data_penaksir (id_penaksir,produk,merek,tipe,warna,versi_android,jenis_bayar,tampilan_depan1,tampilan_depan2,tampilan_depan3,tampilan_belakang1,tampilan_belakang2,tampilan_belakang3,tampilan_belakang4,kelengkapan1,kelengkapan2,kelengkapan3,Kelayakan) VALUES
        ("'.$idpenaksir.'","'.$col.'","'.$layout.'","'.$type.'","'.$warna_desc.'","'.$ver_desc.'","'.$jns_bayar_desc.'","'.$tP1_desc.'","'.$tP2_desc.'","'.$tP3_desc.'","'.$tB1_desc.'","'.$tB2_desc.'","'.$tB3_desc.'","'.$tB4_desc.'","'.$K1_desc.'","'.$K2_desc.'","'.$K3_desc.'","'.$layak.'")';
        mysqli_query($conn2, $sql);   

     echo("<script type='text/javascript'>
     Swal.fire({
                    title: 'Barang Tidak Layak Gadai!',
                    icon: 'error',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK'
               }).then((result) => {
                    if (result.value) {
                         window.location = 'FormKelayakan.php';
                    }
               })
     
          </script>");

    }
    else{
     $layak="Layak";
     $sql = 'INSERT INTO fuzzy_data_penaksir (id_penaksir,produk,merek,tipe,warna,versi_android,jenis_bayar,tampilan_depan1,tampilan_depan2,tampilan_depan3,tampilan_belakang1,tampilan_belakang2,tampilan_belakang3,tampilan_belakang4,kelengkapan1,kelengkapan2,kelengkapan3,Kelayakan) VALUES
     ("'.$idpenaksir.'","'.$col.'","'.$layout.'","'.$type.'","'.$warna_desc.'","'.$ver_desc.'","'.$jns_bayar_desc.'","'.$tP1_desc.'","'.$tP2_desc.'","'.$tP3_desc.'","'.$tB1_desc.'","'.$tB2_desc.'","'.$tB3_desc.'","'.$tB4_desc.'","'.$K1_desc.'","'.$K2_desc.'","'.$K3_desc.'","'.$layak.'")';
     mysqli_query($conn2, $sql);
     
     echo("<script type='text/javascript'>
     Swal.fire({
                 title: 'Barang Layak Gadai!',
                 icon: 'success',
                 confirmButtonColor: '#3085d6',
                 confirmButtonText: 'OK'
               }).then((result) => {
                 if (result.value) {
                       window.location = 'FormKelayakan.php';
                 }
               })
     
      </script>");
     }
    ?>
    
    </body>
</html>
    
     <script>
        $('.select2').select2();
     </script>
     <script src="sweetalert2.all.min.js"></script>
     <!-- Optional: include a polyfill for ES6 Promises for IE11 -->
     <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>

     <script src="js/jquery.js"></script>
     <script src="js/bootstrap.min.js"></script>
     <script src="js/jquery.stellar.min.js"></script>
     <script src="js/wow.min.js"></script>
     <script src="js/owl.carousel.min.js"></script>
     <script src="js/jquery.magnific-popup.min.js"></script>
     <script src="js/smoothscroll.js"></script>
     <script src="js/custom.js"></script>

     
