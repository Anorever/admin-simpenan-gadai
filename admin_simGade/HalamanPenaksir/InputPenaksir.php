<!doctype html> 
<html> 
<head> 
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
  <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

     <link rel="stylesheet" href="css/bootstrap.min.css">
     <link rel="stylesheet" href="css/font-awesome.min.css">
     <link rel="stylesheet" href="css/animate.css">
     <link rel="stylesheet" href="css/owl.carousel.css">
     <link rel="stylesheet" href="css/owl.theme.default.min.css">
     <link rel="stylesheet" href="css/magnific-popup.css">

     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
     <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
     <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

     <!-- MAIN CSS -->
     <link rel="stylesheet" href="css/form-kelayakan.css">
     <?php include 'koneksi.php';?>
  <title>Simpenan Gadai</title> 
	<style>
{box-sizing:border-box}

/* Slideshow container */
.slideshow-container {
  max-width: 700px;
  position: relative;
  /* padding-left:200px; */
  margin: auto;
}


/* Hide the images by default */
.mySlides {
  display: none;
}
.con-form {
  width:500px;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  margin-top: -22px;
  padding: 16px;
  color: black;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px 0 220px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
  
}

.active, .dot:hover {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s ;
  animation-name: fade;
  animation-duration:  1.5s;
  animation-iteration-count: infinite;
}

@-webkit-keyframes fade {
  from {opacity: 1}
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: 1}
  to {opacity: 1}
}
}

.input-icons i { 
            position: absolute; 
        } 
          
        .input-icons { 
            width: 100%; 
            margin-bottom: 10px; 
        } 
          
        .icon { 
            padding: 10px; 
            min-width: 40px; 
        } 
          
        .input-field { 
            width: 100%; 
            padding: 10px; 
            text-align: center; 
        } 
td{
    padding-bottom:5%;
    width:2px;
}

	</style>
</head>
<section id="formKelayakan" class="slider" data-stellar-background-ratio="2"> 
<body> 

     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>

               </div>

               

               <!-- MENU LINKS -->
               <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                         <img src="images/Header_Pegadaian.png" height="8%" width="18%">
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                         <!-- <li><a href="index.php#home" class="smoothScroll">Home</a></li>
                         <li><a href="index.php#contact" class="smoothScroll">Lokasi</a></li> -->
                         <li><a href="FormKelayakan.php" class="smoothScroll"><u>Cek Kelayakan</u></a></li> 
                         <li><a href="InputPenaksir.php" class="smoothScroll"><u>Tabel Input Penaksir</u></a></li>   
                         <li><a href="../logout.php" class="smoothScroll"><u>Logout</u></a></li>                               
                         <!--<a href="#footer" class="section-btn">Login</a>-->
                    </ul>
               </div>

          </div>
     </section>

<div class="container" >
  <div class="row">

   
      <h3 style="padding-bottom:2%; text-align:center;">Tabel Inputan Penaksir</h3>
    <br/>
    <div style="overflow-x:auto; overflow-y:auto;">
    <table class="table table-sm">
      <thead>
        <tr class="table-secondary">
          <th>No</th>
          <th>ID_Penaksir</th>	
          <th>produk</th>	
          <th>merek	</th>
          <th>warna</th>	
          <th>tipe</th>	
          <th>versi_android</th>	
          <th>jenis_bayar</th>	
          <th>tampilan_depan1</th>	
          <th>tampilan_depan2</th>	
          <th>tampilan_depan3</th>	
          <th>tampilan_belakang1</th>	
          <th>tampilan_belakang2</th>	
          <th>tampilan_belakang3</th>	
          <th>tampilan_belakang4</th>	
          <th>kelengkapan1</th>	
          <th>kelengkapan2</th>	
          <th>kelengkapan3</th>	
          <th>Kelayakan</th>
        </tr>
    </thead>
    <tbody>
      <?php
      // jalankan query untuk menampilkan semua data diurutkan berdasarkan nim
      $query = "SELECT * FROM fuzzy_data_penaksir ORDER BY id ASC";
      $result = mysqli_query($koneksi, $query);
      //mengecek apakah ada error ketika menjalankan query
      if(!$result){
        die ("Query Error: ".mysqli_errno($koneksi).
           " - ".mysqli_error($koneksi));
      }

      //buat perulangan untuk element tabel dari data mahasiswa
      $no = 1; //variabel untuk membuat nomor urut
      // hasil query akan disimpan dalam variabel $data dalam bentuk array
      // kemudian dicetak dengan perulangan while
      while($row = mysqli_fetch_assoc($result))
      {
      ?>
       <tr>
          <td><?php echo $no; ?></td>
         
        <td><?php echo $row['ID_Penaksir']; ?></td>	
        <td><?php echo $row['produk']; ?></td>	
        <td><?php echo $row['merek']; ?></td>	
        <td><?php echo $row['warna']; ?></td>	
        <td><?php echo $row['tipe']; ?></td>	
        <td><?php echo $row['versi_android']; ?></td>	
        <td><?php echo $row['jenis_bayar']; ?></td>	
        <td><?php echo $row['tampilan_depan1']; ?></td>	
        <td><?php echo $row['tampilan_depan2']; ?></td>	
        <td><?php echo $row['tampilan_depan3']; ?></td>	
        <td><?php echo $row['tampilan_belakang1']; ?></td>	
        <td><?php echo $row['tampilan_belakang2']; ?></td>	
        <td><?php echo $row['tampilan_belakang3']; ?></td>	
        <td><?php echo $row['tampilan_belakang4']; ?></td>	
        <td><?php echo $row['kelengkapan1']; ?></td>	
        <td><?php echo $row['kelengkapan2']; ?></td>	
        <td><?php echo $row['kelengkapan3']; ?></td>	
        <td><?php echo $row['Kelayakan']; ?></td>
          
      </tr>
         
      <?php
        $no++; //untuk nomor urut terus bertambah 1
      }
      ?>
    </tbody>
    </table>
   
    </div>
 </div>
</div>

    <!-- FOOTER -->
    <footer id="footer" data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row">

                    <div class="col-md-4 col-sm-8">
                         <div class="footer-info">
                              <div class="section-title">
                              <h5 class="font-weight-bold text-uppercase mb-2 wow fadeInUp" data-wow-delay="0.2s" >SIMPENAN GADAI</h5>
                              <p  class="wow fadeInUp" data-wow-delay="0.2s"><em>"Sistem Pengambil Keputusan Gadai"</em></p>
                              <p  class="wow fadeInUp" data-wow-delay="0.2s">Tugas Mini Project - SPRINT Angakatan 1 2020</p>
                              </div>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-8">
                         <div class="footer-info">
                              <div class="section-title">
                                   <h2 class="wow fadeInUp" data-wow-delay="0.2s">Alamat</h2>
                              </div>
                              <ul class="list-unstyled wow fadeInUp" data-wow-delay="0.2s">
                                   <li>
                                   <p>
                                        <i class="fas fa-home mr-3"></i> Kantor Pusat: Jl. Kramat Raya 162 Jakarta Pusat 10430 </p>
                                   </li>
                                   <li>
                                   <p>
                                        <i class="fas fa-envelope mr-3"></i> pegadaian@pegadaian.co.id</p>
                                   </li>
                                   <li>
                                   <p>
                                        <i class="fas fa-phone mr-3"></i> 0213155550 | 02180635162 </p>
                                   </li>
                              </ul>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                         <br>
                         <h5 class="font-weight-bold text-uppercase mb-2 wow fadeInUp" style="padding-left:35%;" data-wow-delay="0.8s">Follow Us</h5><br>
                              <a type="button" class="btn-floating btn-fb fa-2x wow fadeInUp" style="padding-left:35%;" data-wow-delay="0.8s">
                              <i class="fab fa-facebook-f ico"></i>
                              </a> 
                              <a type="button" class="btn-floating btn-tw fa-2x wow fadeInUp" data-wow-delay="0.8s">
                                   <i class="fab fa-twitter ico"></i>
                              </a>
                              <a type="button" class="btn-floating btn-instagram fa-2x wow fadeInUp" data-wow-delay="0.8s">
                                   <i class="fab fa-instagram ico"></i>
                              </a>
                         <div class="wow fadeInUp copyright-text" data-wow-delay="0.8s"> 
                              <p style="padding-left:20%;"><br>Copyright 2020 &copy; Team4SPRINT
                         </div>
                    </div>
                    
               </div>
          </div>
     </footer>
<script src="js/jquery.js"></script>
     <script src="js/bootstrap.min.js"></script>
     <script src="js/jquery.stellar.min.js"></script>
     <script src="js/wow.min.js"></script>
     <script src="js/owl.carousel.min.js"></script>
     <script src="js/jquery.magnific-popup.min.js"></script>
     <script src="js/smoothscroll.js"></script>
     <script src="js/custom.js"></script>
  <script src="assets/js/jquery.js"></script> 
	<script src="assets/js/popper.js"></script> 
	<script src="assets/js/bootstrap.js"></script>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  
<script src="http://code.jquery.com/jquery-1.8.1.min.js"></script>
      
                                        
</body> 
</html>
