<?php 
    error_reporting(0);
	session_start();
	if($_SESSION['status']!="login"){
        echo "<script>alert('Anda Belum Login!');window.location='./login.php';</script>";
	}
    ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>SIMPENAN GADE DASHBOARD</title>
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="index.php">SIMPENAN GADAI</a><button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button
            ><!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto ml-md-0">
            <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <!-- <a class="dropdown-item" href="#">Settings</a><a class="dropdown-item" href="#">Activity Log</a>
                        <div class="dropdown-divider"></div> -->
                        <a class="dropdown-item" href="../logout.php">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Monitoring</div>
                            <a class="nav-link" href="index.php"
                                ><div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Traffic Visitor</a
                            >
                            <a class="nav-link" href="trends.php"
                                ><div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Traffic Trends</a
                            >
                            <div class="sb-sidenav-menu-heading">Data Update</div>
                            <a class="nav-link" href="grafik.php"
                                ><div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                                HPS Grafik</a
                            ><a class="nav-link" href="data2.php"
                                ><div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Data Input</a
                            >
                            <div class="sb-sidenav-menu-heading">GIST</div>
                            <a class="nav-link" href="crud/user_config.php"
                                ><div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Management User</a
                            >
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        SimGade-Admin
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Harga Pasar Setempat</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item active"></li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">Penurunan harga produk dari Harga Pasar setempat</div>
                        </div>
                        <div class="card mb-4">
                            <div class="card-header"><i class="fas fa-chart-area mr-1"></i>Harga Pasar Setempat Iphone Pro 11</div>
                            <div class="card-header">
                                <!-- <select id="HP">
                                    <option value="Iphone Pro 11">Iphone 7</option>
                                    <option value="saab">Samsung A30S</option>
                                    <option value="mercedes">Oppo </option>
                                    <option value="audi">Audi</option>
                                </select>
                                <script>
                                
                                $(document).ready(function () {
                                    $('.box').hide();
                                    $('#1').show();
                                    $('#Field').change(function () {
                                        $('.box').hide();
                                        $('#' + $(this).val()).show();
                                    });
                                });  
                                
                                </script>
                                <select name="size" id="Field">
                                    <option value="1">Small</option>
                                    <option value="2">Medium</option>
                                    <option value="3">Large</option>
                                </select>
                                 -->
                                <!-- <div id="1" class="box">4</div>
                                <div id="2" class="box">30</div>
                                <div id="3" class="box">54</div> -->

                            <div class="card-body"><div id="chartContainer" style="height: 370px; width: 100%;"></div>
                            <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Team4Sprint</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script>
            window.onload = function () {
            
            var dataPoints = [];
            
            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                exportEnabled: true,
                title:{
                    text: "Harga Pasar Setempat"
                },
                axisY: {
                    title: "Rupiah",
                    includeZero: false
                },
                data: [{
                    type: "line",
                    toolTipContent: "tanggal",
                    dataPoints: dataPoints
                }]
            });
            
            $.get("Test_without_Filter2.csv", getDataPointsFromCSV);
            
            //CSV Format
            //Year,Volume
            function getDataPointsFromCSV(csv) {
                var csvLines = points = [];
                csvLines = csv.split(/[\r?\n|\r|\n]+/);
                for (var i = 0; i < csvLines.length; i++) {
                    if (csvLines[i].length > 0) {
                        points = csvLines[i].split(",");
                        dataPoints.push({
                            label: points[0],
                            y: parseFloat(points[1])
                        });
                    }
                }
                chart.render();
            }
            
            }
            

            // var elem = document.getElementById("security_question_1");
            // elem.onchange = function(){
            //     var hiddenDiv = document.getElementById("chartContainer");
            //     hiddenDiv.style.display = (this.value == "") ? "none":"block";
            // };

            </script>
        <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
        <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/chart-area-demo2.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script>
        <script src="assets/demo/chart-pie-demo.js"></script>
    </body>
</html>
